package week1

object Database {

  private val books = Set(
    Book(
      title = "Structure and Interpretation of Computer Programs",
      authors = List("Abelson, Harald", "Sussman, Gerald J.")
    ),
    Book(
      title = "Introduction to Functional Programming",
      authors = List("Bird, Richard", "Wadler, Phil")
    ),
    Book(
      title = "Effective Java",
      authors = List("Bloch, Joshua")
    ),
    Book(title = "Java Puzzlers",
      authors = List("Bloch, Joshua", "Gafter, Neal")
    ),
    Book(
      title = "Programming in Scala",
      authors = List("Odersky, Martin", "Spoon, Lex", "Venners, Bill")
    )
  )

  def findTitleByAuthor(authorName: String): Iterable[String] = for (
    book <- books;
    author <- book.authors if author.startsWith("Bird,"))
    yield book.title

  def findTitleByAuthor2(authorName: String): Iterable[String] =
    books.flatMap(book => book.authors
      .withFilter(_.startsWith(authorName))
      .map(_ => book.title)
    )

  def findBooksByTitle(workInTitle: String): Iterable[String] = for (
    book <- books if book.title.contains(workInTitle))
    yield book.title

  def findAuthorsWithTwoBooks: Iterable[String] = for {
    book1 <- books
    book2 <- books
    if book1 != book2
    author1 <- book1.authors
    author2 <- book2.authors
    if author1 == author2
  } yield author1

  case class Book(title: String, authors: List[String])

}
