package week1


trait Generator[+T] {
  def generate: T

  def map[S](f: T => S): Generator[S] = new Generator[S] {
    override def generate: S = f(Generator.this.generate)
  }

  def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
    override def generate: S = f(Generator.this.generate).generate
  }
}

trait Tree

case class Inner(left: Tree, right: Tree) extends Tree

case class Leaf(data: Int) extends Tree

object Generator {

  val integers: Generator[Int] = new Generator[Int] {
    val rand = new java.util.Random

    override def generate: Int = rand.nextInt()
  }

  val booleans: Generator[Boolean] = for (i <- integers) yield i % 2 == 0

  def pairs[T, U](t: Generator[T], u: Generator[U]): Generator[(T, U)] = for {
    a <- t
    b <- u
  } yield (a, b)

  def single[T](x: T): Generator[T] = new Generator[T] {
    override def generate: T = x
  }

  def oneOf[T](xs: T*): Generator[T] = {
    choose(0, xs.length).map(i => xs(i))
  }

  def choose(lo: Int, hi: Int): Generator[Int] = {
    for (i <- integers) yield math.abs(lo + i % (hi - lo))
  }

  def lists: Generator[List[Int]] = for {
    isEmpty <- booleans
    list <- if (isEmpty) emptyLists else nonEmptyLists
  } yield list

  def emptyLists: Generator[List[Int]] = single(Nil)

  def nonEmptyLists: Generator[List[Int]] = for {
    head <- integers
    tail <- lists
  } yield head :: tail

  def leafs: Generator[Leaf] = for {
    data <- integers
  } yield Leaf(data)

  def inners: Generator[Inner] = for {
    left <- trees
    right <- trees
  } yield Inner(left, right)


  def trees: Generator[Tree] = for {
    isLeaf <- booleans
    tree <- if (isLeaf) leafs else inners
  } yield tree

  def test[T](g: Generator[T], numTimes: Int = 100)(testFunc: T => Boolean): Unit = {
    0.until(numTimes).foreach(i => {
      val value = g.generate
      assert(testFunc(value), s"$i - test failed for: $value")
    })
    println(s"test passed, number of times: $numTimes")
  }

}