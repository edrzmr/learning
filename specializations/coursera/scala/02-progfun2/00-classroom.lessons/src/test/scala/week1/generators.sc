import week1.Generator._

integers.generate
integers.generate

booleans.generate
booleans.generate

pairs(integers, booleans).generate
pairs(integers, booleans).generate

single(1).generate
single(1).generate

choose(10, 100).generate
choose(1, 10).generate


lists.generate
lists.generate

trees.generate
trees.generate

test(pairs(lists, lists)) {
  case (xs, ys) =>
    println(xs)
    println(ys)
    true
}

