ThisBuild / scalaVersion := "2.12.6"
ThisBuild / version := "0.0.1"

val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"

lazy val root = (project in file("."))
  .settings(
    name := "Classroom Lessons",
    libraryDependencies += scalaTest % Test,
  )