package calculator

object Polynomial extends PolynomialInterface {
  def computeDelta(a: Signal[Double],
                   b: Signal[Double],
                   c: Signal[Double]): Signal[Double] = {
    Signal {
      math.pow(b(), 2) - 4 * a() * c()
    }
  }

  def computeSolutions(a: Signal[Double],
                       b: Signal[Double],
                       c: Signal[Double],
                       delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      val dd = delta()
      if (dd < 0) Set.empty
      else {
        val aa = a()
        val bb = b()
        if (dd == 0) Set(-bb / 2 * aa)
        else Set(
          (-bb - math.sqrt(dd)) / (2 * aa),
          (-bb + math.sqrt(dd)) / (2 * aa)
        )
      }
    }
  }
}
