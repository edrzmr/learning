package scalashop

import java.util.concurrent._
import scala.collection._
import org.junit._
import org.junit.Assert.assertEquals

class BlurSuite {

  @Rule def individualTestTimeout = new org.junit.rules.Timeout(10 * 1000)

  val i3x4: Img = {
    val i = new Img(3, 4)
    i(0, 0) = 0x00
    i(1, 0) = 0x01
    i(2, 0) = 0x02
    i(0, 1) = 0x03
    i(1, 1) = 0x04
    i(2, 1) = 0x05
    i(0, 2) = 0x06
    i(1, 2) = 0x07
    i(2, 2) = 0x08
    i(0, 3) = 0x09
    i(1, 3) = 0x0A
    i(2, 3) = 0x0B
    i
  }

  @Test def `boxBlurKernel should return the correct value on 3x4 image with radius 1`(): Unit = {
    val i = i3x4

    /* row 0 */
    assertEquals(2, boxBlurKernel(i, 0, 0, 1))
    assertEquals(2, boxBlurKernel(i, 1, 0, 1))
    assertEquals(3, boxBlurKernel(i, 2, 0, 1))
    /* row 1 */
    assertEquals(3, boxBlurKernel(i, 0, 1, 1))
    assertEquals(4, boxBlurKernel(i, 1, 1, 1))
    assertEquals(4, boxBlurKernel(i, 2, 1, 1))

    /* row 2 */
    assertEquals(6, boxBlurKernel(i, 0, 2, 1))
    assertEquals(7, boxBlurKernel(i, 1, 2, 1))
    assertEquals(7, boxBlurKernel(i, 2, 2, 1))

    /* row 3 */
    assertEquals(8, boxBlurKernel(i, 0, 3, 1))
    assertEquals(8, boxBlurKernel(i, 1, 3, 1))
    assertEquals(9, boxBlurKernel(i, 2, 3, 1))
  }

  @Test def `buildStrips should works`(): Unit = {
    assertEquals(Vector((0, 1), (1, 2)), buildStrips(2, 32))
    assertEquals(Vector((0, 1), (1, 2), (2, 3)), buildStrips(3, 32))
    assertEquals(Vector((0, 3)), buildStrips(3, 1))
  }

  @Test def `VerticalBoxBlur.blur should works on 3x4 image with radius 1`(): Unit = {
    val src = i3x4
    val dst = new Img(src.width, src.height)

    VerticalBoxBlur.blur(src, dst, 0, src.width, 1)

    assertEquals(2, dst(0, 0))
    assertEquals(2, dst(1, 0))
    assertEquals(3, dst(2, 0))
    assertEquals(3, dst(0, 1))
    assertEquals(4, dst(1, 1))
    assertEquals(4, dst(2, 1))
    assertEquals(6, dst(0, 2))
    assertEquals(7, dst(1, 2))
    assertEquals(7, dst(2, 2))
    assertEquals(8, dst(0, 3))
    assertEquals(8, dst(1, 3))
    assertEquals(9, dst(2, 3))
  }

  @Test def `VerticalBoxBlur.parBlur should works on 3x4 image with radius 1`(): Unit = {
    val src = i3x4
    val dst = new Img(src.width, src.height)

    VerticalBoxBlur.parBlur(src, dst, 2, 1)

    assertEquals(2, dst(0, 0))
    assertEquals(2, dst(1, 0))
    assertEquals(3, dst(2, 0))
    assertEquals(3, dst(0, 1))
    assertEquals(4, dst(1, 1))
    assertEquals(4, dst(2, 1))
    assertEquals(6, dst(0, 2))
    assertEquals(7, dst(1, 2))
    assertEquals(7, dst(2, 2))
    assertEquals(8, dst(0, 3))
    assertEquals(8, dst(1, 3))
    assertEquals(9, dst(2, 3))
  }

  @Test def `VerticalBoxBlur.parBlur should blur all pixels`(): Unit = {
    val width = 32
    val height = 64

    val src = new Img(width, height)
    val dst = new Img(width, height)

    for (row <- 0 until height; col <- 0 until width) {
      src(col, row) = 50
      dst(col, row) = 0
    }

    VerticalBoxBlur.parBlur(src, dst, 32, 1)

    for (row <- 0 until height; col <- 0 until width) {
      assertEquals(src(col, row), dst(col, row))
    }
  }

  @Test def `HorizontalBoxBlur.parBlur should blur all pixels`(): Unit = {
    val width = 32
    val height = 64

    val src = new Img(width, height)
    val dst = new Img(width, height)

    for (row <- 0 until height; col <- 0 until width) {
      src(col, row) = 50
      dst(col, row) = 0
    }

    HorizontalBoxBlur.parBlur(src, dst, 32, 1)

    for (row <- 0 until height; col <- 0 until width) {
      assertEquals(src(col, row), dst(col, row))
    }
  }
}
