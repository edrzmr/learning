package observatory

import org.junit.Assert._
import org.junit.Test

trait VisualizationTest extends MilestoneSuite {
  private val milestoneTest = namedMilestoneTest("raw data display", 2) _

  // Implement tests for the methods of the `Visualization` object

  @Test def `v1 - Visualization.predictTemperature works for sample data`(): Unit = {
    val testData: Iterable[(Location, Temperature)] = Seq(
      (Location(10, 10), 20.0),
      (Location(20, 20), 10.0)
    )

    val expectedResults: Iterable[(Location, Temperature)] = Seq(
      (Location(10, 10), 20.0),
      (Location(20, 20), 10.0),
      (Location(15, 15), 14.94346697),
      (Location(-10, -10), 16.90043206)
    )

    for ((testLocation, expectedTemperature) <- expectedResults) {
      assertEquals(
        "Unexpected predicted temperature for " + testLocation,
        expectedTemperature,
        Visualization.predictTemperature(testData, testLocation),
        1e-8
      )
    }
  }

  @Test def `v2 - Visualization.interpolateColor works for sample data`(): Unit = {
    val testColors: Iterable[(Temperature, Color)] = Seq(
      (60, Color(255, 255, 255)),
      (32, Color(255, 0, 0)),
      (12, Color(255, 255, 0)),
      (0, Color(0, 0, 255)),
      (-15, Color(0, 0, 255)),
      (-27, Color(255, 0, 255)),
      (-50, Color(33, 0, 107)),
      (-60, Color(0, 0, 5))
    )

    val expectedResults: Iterable[(Temperature, Color)] = Seq(
      (6, Color(128, 128, 128)),
      (0, Color(0, 0, 255)),
      (60, Color(255, 255, 255)),
      (61, Color(255, 255, 255)),
      (-60, Color(0, 0, 5)),
      (-61, Color(0, 0, 5))
    )

    for ((testTemperature, expectedColor) <- expectedResults) {
      assertEquals(
        "Unexpected interpolated color for " + testTemperature,
        expectedColor,
        Visualization.interpolateColor(testColors, testTemperature)
      )
    }
  }

  @Test def `v3 - Visualization.visualize works for sample data`(): Unit = {
    val testTemperatures: Iterable[(Location, Temperature)] = Seq(
      (Location(0, 0), 50.0),
      (Location(20, 20), 10.0),
      (Location(50, -30), -20.0),
      (Location(-85, 5), -50.0),
      (Location(85, -5), -50.0)
    )

    val testColors: Iterable[(Temperature, Color)] = Seq(
      (60, Color(255, 255, 255)),
      (32, Color(255, 0, 0)),
      (12, Color(255, 255, 0)),
      (0, Color(0, 0, 255)),
      (-15, Color(0, 0, 255)),
      (-27, Color(255, 0, 255)),
      (-50, Color(33, 0, 107)),
      (-60, Color(0, 0, 0))
    )

    val testWidth  = 360
    val testHeight = 180
    val image      = Visualization.visualize(testTemperatures, testColors)
    assertEquals("Unexpected image dimensions", (testWidth, testHeight), (image.width, image.height))
  }

  @Test def `v4 - Distance simple test`(): Unit = {
    val a = Location(5, 10)
    val b = Location(-5, 10)

    assert(Visualization.greatCircleDistance(a, a) == 0)
    assert(Visualization.greatCircleDistance(a, b) > 0)
  }

  @Test def `v5 - Temperatures interpolation test`(): Unit = {
    val temps = List[(Location, Temperature)](
      (Location(5, 5), 3),
      (Location(-5, 5), 5)
    )

    val c1 = Visualization.predictTemperature(temps, Location(5, 5))
    assert(c1 == 3)

    val c2 = Visualization.predictTemperature(temps, Location(0, 3))
    assert(c2 == 4)

    val c3 = Visualization.predictTemperature(temps, Location(4, 4))
    assert(c3 < 4)
  }

  @Test def `v6 - Color interpolate one val`(): Unit = {
    val cols  = List[(Temperature, Color)]((1, Color(2, 2, 2)))
    val value = 2

    val computed = Visualization.interpolateColor(cols, value)
    assert(computed == Color(2, 2, 2))
  }

  @Test def `v7 - Color interpolate two val`(): Unit = {
    val cols = List[(Temperature, Color)](
      (1, Color(2, 2, 2)),
      (-1, Color(4, 4, 4))
    )

    val c1 = Visualization.interpolateColor(cols, 1)
    assert(c1 == Color(2, 2, 2))

    val c2 = Visualization.interpolateColor(cols, 0)
    assert(c2 == Color(3, 3, 3))
  }

  @Test def `v8 - Color interpolate two val`(): Unit = {
    val cols = List[(Temperature, Color)](
      (-1d, Color(255, 0, 0)),
      (0d, Color(0, 0, 255))
    )

    val c1 = Visualization.interpolateColor(cols, -0.75d)
    println(c1)
    assert(c1 == Color(191, 0, 64))

    //val c2 = Visualization.interpolateColor(cols, 0)
    //assert(c2 == Color(3, 3, 3))
  }

}
