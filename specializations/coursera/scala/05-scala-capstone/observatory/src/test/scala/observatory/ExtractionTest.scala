package observatory

import observatory.Extraction.parseStation
import observatory.Extraction.resourceToRdd
import org.junit.Test

trait ExtractionTest extends MilestoneSuite {
  private val milestoneTest = namedMilestoneTest("data extraction", 1) _

  // Implement tests for the methods of the `Extraction` object

  @Test def `1 - parseStation should be None with "010013,,," as input`(): Unit =
    assert(parseStation("010013,,,").isEmpty)

  @Test def `2 - parseStation should be Some with "010040,,+78.917,+011.933" as input`(): Unit =
    assert(
      parseStation("010040,,+78.917,+011.933") == Some(StationID(Some("010040"), None), Location(78.917d, 011.933d)))

  @Test def `3 - parseStation should be Some with "724017,03707,+37.358,-078.438" as input`(): Unit =
    assert(
      parseStation("724017,03707,+37.358,-078.438") ==
        Some(StationID(Some("724017"), Some("03707")), Location(37.358d, -78.438d)))

  @Test def `4 - parseStation should be Some with "724017,,+37.350,-078.433" as input`(): Unit =
    assert(
      parseStation("724017,,+37.350,-078.433") ==
        Some(StationID(Some("724017"), None), Location(37.350d, -78.433d)))

  @Test def `5 - resourceToRdd should load "/stations-mock.csv"`(): Unit = {

    val stations = resourceToRdd("/stations-test.csv")(line => parseStation(line)).collect()

    val expected = Array(
      (StationID(Some("007018"), None), Location(0.0, 0.0)),
      (StationID(Some("008268"), None), Location(32.95, 65.567)),
      (StationID(Some("788660"), Some("00399")), Location(18.046, -63.115)),
      (StationID(None, Some("91004")), Location(80.0, -113.0))
    )
    assert(stations.sameElements(expected))
  }

}
