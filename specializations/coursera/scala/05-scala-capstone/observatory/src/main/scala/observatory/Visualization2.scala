package observatory

import com.sksamuel.scrimage.Image
import com.sksamuel.scrimage.Pixel

/**
  * 5th milestone: value-added information visualization
  */
object Visualization2 extends Visualization2Interface {

  val alpha       = 256
  val subTileZoom = 8
  val width: Int  = 1 << subTileZoom // 2 ^ subTileZoom == 256
  val height: Int = 1 << subTileZoom // 2 ^ subTileZoom == 256

  /**
    * @param point (x, y) coordinates of a point in the grid cell
    * @param d00 Top-left value
    * @param d01 Bottom-left value
    * @param d10 Top-right value
    * @param d11 Bottom-right value
    * @return A guess of the value at (x, y) based on the four known values, using bilinear interpolation
    *         See https://en.wikipedia.org/wiki/Bilinear_interpolation#Unit_Square
    */
  def bilinearInterpolation(
      point: CellPoint,
      d00: Temperature,
      d01: Temperature,
      d10: Temperature,
      d11: Temperature
  ): Temperature = {
    val (x, y) = (point.x, point.y)

    d00 * (1 - x) * (1 - y) +
      d10 * x * (1 - y) +
      d01 * (1 - x) * y +
      d11 * x * y
  }

  def interpolate(location: Location, predictTemperature: GridLocation => Temperature): Temperature = {
    val lat = location.lat.toInt
    val lon = location.lon.toInt

    bilinearInterpolation(
      point = CellPoint(location.lon - lon, location.lat - lat),
      d00 = predictTemperature(GridLocation(lat, lon)),
      d01 = predictTemperature(GridLocation(lat + 1, lon)),
      d10 = predictTemperature(GridLocation(lat, lon + 1)),
      d11 = predictTemperature(GridLocation(lat + 1, lon + 1))
    )
  }

  /**
    * @param grid Grid to visualize
    * @param colors Color scale to use
    * @param tile Tile coordinates to visualize
    * @return The image of the tile at (x, y, zoom) showing the grid using the given color scale
    */
  def visualizeGrid(
      grid: GridLocation => Temperature,
      colors: Iterable[(Temperature, Color)],
      tile: Tile
  ): Image = {

    val coordinates = for {
      i <- 0 until width
      j <- 0 until height
    } yield (i, j)

    val xOffset = tile.x * width
    val yOffset = tile.y * height
    val zOffset = tile.zoom

    val pixels: Array[Pixel] =
      coordinates.par.map { case (i, j) => Tile(j + xOffset, i + yOffset, subTileZoom + zOffset) }
        .map(Interaction.tileLocation)
        .map(location => interpolate(location, grid))
        .map(Visualization.interpolateColor(colors, _))
        .map(color => Pixel(color.red, color.green, color.blue, alpha))
        .toArray

    Image(width, height, pixels)
  }

}
