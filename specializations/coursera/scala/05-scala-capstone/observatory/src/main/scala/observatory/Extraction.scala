package observatory

import java.time.LocalDate

import scala.io.Source._
import scala.reflect.ClassTag

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

/**
  * 1st milestone: data extraction
  */
object Extraction extends ExtractionInterface {

  lazy val conf: SparkConf = new SparkConf()
    .setMaster("local[2]")
    .setAppName("Observatory Extraction")

  lazy val sc: SparkContext = new SparkContext(conf)

  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(year: Year,
                         stationsFile: String,
                         temperaturesFile: String): Iterable[(LocalDate, Location, Temperature)] =
    locateTemperaturesSpark(
      resourceToRdd(stationsFile)(line => parseStation(line)),
      resourceToRdd(temperaturesFile)(line => parseTemperature(year, line))
    ).collect()

  def locateTemperaturesSpark(
      stationRdd: RDD[(StationID, Location)],
      tempRdd: RDD[(StationID, (LocalDate, Temperature))]): RDD[(LocalDate, Location, Temperature)] =
    stationRdd
      .join(tempRdd)
      .map {
        case (_, (location, (localDate, temperature))) => (localDate, location, temperature)
      }

  def resourceToRdd[T: ClassTag](resource: String)(parser: String => TraversableOnce[T]): RDD[T] =
    sc.parallelize(
        fromInputStream(getClass.getResourceAsStream(resource), "utf-8")
          .getLines()
          .toSeq)
      .flatMap(parser)

  def parseStation(line: String): Option[(StationID, Location)] = parseStation(line.split(","))

  def parseTemperature(year: Year, line: String): Option[(StationID, (LocalDate, Temperature))] =
    parseTemperature(year, line.split(","))

  def parseStation(line: Array[String]): Option[(StationID, Location)] =
    if (line.length != 4) None
    else if (line(2).isEmpty || line(3).isEmpty) None
    else Some((StationID.from(line(0), line(1)), Location.from(line(2), line(3))))

  def parseTemperature(year: Year, line: Array[String]): Option[(StationID, (LocalDate, Temperature))] =
    if (line.length != 5) None
    else
      Some(
        (
          StationID.from(line(0), line(1)),
          (
            Date.from(year, line(2), line(3)),
            Temperature.fromFahrenheit(line(4))
          )
        ))

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */
  def locationYearlyAverageRecords(
      records: Iterable[(LocalDate, Location, Temperature)]): Iterable[(Location, Temperature)] =
    locationYearlyAverageRecordsSpark(sc.parallelize(records.toSeq)).collect()

  def locationYearlyAverageRecordsSpark(rdd: RDD[(LocalDate, Location, Temperature)]): RDD[(Location, Temperature)] =
    rdd.map { case (_, location, temp) => (location, temp) }
      .aggregateByKey(TempAcc.empty)(
        seqOp = (acc, temp) => acc.inc(temp),
        combOp = (acc1, acc2) => acc1.inc(acc2)
      )
      .mapValues(acc => acc.temp / acc.count)

}
