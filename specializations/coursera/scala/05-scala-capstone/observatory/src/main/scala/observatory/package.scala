package object observatory {
  type Temperature = Double // °C, introduced in Week 1
  type Year        = Int    // Calendar year, introduced in Week 1

  type Distance = Double
  type Weight   = Double

  type DistTemp = (Double, Double)

  val earthRadius: Double = 6371.009D
  val width               = 360
  val height              = 180
}
