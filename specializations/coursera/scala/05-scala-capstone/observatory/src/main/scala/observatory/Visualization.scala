package observatory

import com.sksamuel.scrimage.Image
import com.sksamuel.scrimage.Pixel

import scala.math.Pi
import scala.math.abs
import scala.math.acos
import scala.math.pow

/**
  * 2nd milestone: basic visualization
  */
object Visualization extends VisualizationInterface {

  /*
   * you can use any p value greater or equal to 2; try and use whatever works best for you!
   */
  val IDWPParam = 2

  def isAntipode(a: Location, b: Location): Boolean = (a.lat == -b.lat) && (abs(a.lon - b.lon) == 180)

  /*
    for equal points: 0
    for antipodes: Pi
    otherwise: arcCos(sin(f1)) * sin(f2) + cos(f1) * cos(f2) * cos(deltaLambda)
   */
  def greatCircleDistance(a: Location, b: Location): Distance = (a, b) match {
    case (a, b) if a == b           => 0
    case (a, b) if isAntipode(a, b) => earthRadius * Pi
    case (a, b)                     => earthRadius * calcGreatCircleDistance(a.asRadians, b.asRadians)
  }

  def calcGreatCircleDistance(a: RadiansLocation, b: RadiansLocation): Distance =
    acos(a.latSin * b.latSin + a.latCos * b.latCos * math.cos(a.deltaLon(b)))

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Temperature)], location: Location): Temperature = {

    val distTemp: Iterable[(Distance, Temperature)] = temperatures.map {
      case (otherLocation, temp) =>
        (greatCircleDistance(otherLocation, location), temp)
    }

    /*
     * Running the inverse distance weighting algorithm with small distances will result in huge numbers (since we
     * divide by the distance raised to the power of p), which can be a problem. A solution to this problem is to
     * directly use the known temperature of the close (less than 1 km) location as a prediction.
     */
    distTemp.find { case (dist, _) => dist <= 1 } match {
      case Some((_, temp)) => temp
      case None            => inverseDistanceWeighting(distTemp)
    }
  }

  def inverseDistanceWeighting(distTemps: Iterable[(Distance, Temperature)]): Temperature = {

    def weighting(dist: Distance): Distance = 1 / pow(dist, IDWPParam)

    def seqOp(acc: IDWAcc, dt: DistTemp): IDWAcc = dt match {
      case (dist, temp) => acc.inc(weighting(dist), temp)
    }

    def combOp(a: IDWAcc, b: IDWAcc): IDWAcc = a.combine(b)

    val IDWAcc(numerator, denominator) = distTemps.aggregate(IDWAcc.empty)(seqOp, combOp)

    numerator / denominator
  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Temperature, Color)], value: Temperature): Color =
    points.find(_._1 == value) match {
      case Some((_, color)) => color
      case None =>
        points.partition(_._1 < value) match {
          case (colder, warmer) if colder.isEmpty => warmer.minBy(_._1)._2
          case (colder, warmer) if warmer.isEmpty => colder.maxBy(_._1)._2
          case (colder, warmer) =>
            val (maxTemp, maxColor) = colder.maxBy(_._1)
            val (minTemp, minColor) = warmer.minBy(_._1)
            val interpolate         = linearInterpolation(maxTemp, minTemp, value) _
            Color(
              red = interpolate(maxColor.red, minColor.red),
              green = interpolate(maxColor.green, minColor.green),
              blue = interpolate(maxColor.blue, minColor.blue)
            )
        }
    }

  def linearInterpolation(maxTemp: Temperature, minTemp: Temperature, value: Temperature)(maxColor: Int,
                                                                                          minColor: Int): Int = {
    val factor = (value - minTemp) / (maxTemp - minTemp)
    math.round(minColor + (maxColor - minColor) * factor).toInt
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)]): Image = {

    val pixels: Array[Pixel] = new Array[Pixel](width * height)

    for {
      y <- (0 until height).par
      x <- (0 until width).par
      location    = Location.fromPoint(x, y)
      temperature = predictTemperature(temperatures, location)
      color       = interpolateColor(colors, temperature)
    } {
      pixels(x + y * width) = Pixel(r = color.red, g = color.green, b = color.blue, alpha = 255)
    }

    Image(width, height, pixels)
  }

}
