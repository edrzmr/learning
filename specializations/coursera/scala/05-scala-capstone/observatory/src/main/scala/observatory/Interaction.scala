package observatory

import com.sksamuel.scrimage.Image
import com.sksamuel.scrimage.Pixel

/**
  * 3rd milestone: interactive visualization
  */
object Interaction extends InteractionInterface {

  val alpha       = 127
  val subTileZoom = 8
  val width: Int  = 1 << subTileZoom // 2 ^ subTileZoom == 256
  val height: Int = 1 << subTileZoom // 2 ^ subTileZoom == 256

  /**
    * @param tile Tile coordinates
    * @return The latitude and longitude of the top-left corner of the tile, as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    */
  def tileLocation(tile: Tile): Location = {
    import math._
    val n   = pow(2, tile.zoom)
    val lat = toDegrees(atan(sinh(Pi - (tile.y * 2 * Pi) / n)))
    val lon = (tile.x * 360) / n - 180
    Location(lat, lon)
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @param tile Tile coordinates
    * @return A 256×256 image showing the contents of the given tile
    */
  def tile(temperatures: Iterable[(Location, Temperature)],
           colors: Iterable[(Temperature, Color)],
           tile: Tile): Image = {

    val coordinates = for {
      i <- 0 until width
      j <- 0 until height
    } yield (i, j)

    val xOffset = tile.x * width
    val yOffset = tile.y * height
    val zOffset = tile.zoom

    val pixels: Array[Pixel] =
      coordinates.par.map { case (i, j) => Tile(j + xOffset, i + yOffset, subTileZoom + zOffset) }
        .map(tileLocation)
        .map(Visualization.predictTemperature(temperatures, _))
        .map(Visualization.interpolateColor(colors, _))
        .map(color => Pixel(color.red, color.green, color.blue, alpha))
        .toArray

    Image(width, height, pixels)
  }

  /**
    * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
    * @param yearlyData Sequence of (year, data), where `data` is some data associated with
    *                   `year`. The type of `data` can be anything.
    * @param generateImage Function that generates an image given a year, a zoom level, the x and
    *                      y coordinates of the tile and the data to build the image from
    */
  def generateTiles[Data](
      yearlyData: Iterable[(Year, Data)],
      generateImage: (Year, Tile, Data) => Unit
  ): Unit =
    for {
      zoom         <- 0 until 4
      x            <- 0 until (1 << zoom)
      y            <- 0 until (1 << zoom)
      (year, data) <- yearlyData
    } generateImage(year, Tile(x, y, zoom), data)
}
