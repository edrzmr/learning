package observatory

import scala.collection.parallel.ParIterable

/**
  * 4th milestone: value-added information
  */
object Manipulation extends ManipulationInterface {

  /**
    * @param temperatures Known temperatures
    * @return A function that, given a latitude in [-89, 90] and a longitude in [-180, 179],
    *         returns the predicted temperature at this location
    */
  def makeGrid(temperatures: Iterable[(Location, Temperature)]): GridLocation => Temperature = {

    val latRange = -89 to 90
    val lonRange = -180 to 179

    val memoize: Map[GridLocation, Temperature] = {
      for {
        lat <- latRange
        lon <- lonRange
        temperature = Visualization.predictTemperature(temperatures, Location(lat, lon))
      } yield GridLocation(lat, lon) -> temperature
    }.toMap

    def predictTemperature(location: GridLocation): Temperature = memoize(location)

    predictTemperature
  }

  /**
    * @param temperaturess Sequence of known temperatures over the years (each element of the collection
    *                      is a collection of pairs of location and temperature)
    * @return A function that, given a latitude and a longitude, returns the average temperature at this location
    */
  def average(temperaturess: Iterable[Iterable[(Location, Temperature)]]): GridLocation => Temperature = {
    val gridPerYear: ParIterable[GridLocation => Temperature] = temperaturess.par.map(temps => makeGrid(temps))

    def predictAverageTemperature(location: GridLocation): Temperature = {
      val temps   = gridPerYear.map(predictTemperature => predictTemperature(location))
      val average = temps.sum / temps.size
      average
    }

    predictAverageTemperature
  }

  /**
    * @param temperatures Known temperatures
    * @param normals A grid containing the “normal” temperatures
    * @return A grid containing the deviations compared to the normal temperatures
    */
  def deviation(temperatures: Iterable[(Location, Temperature)],
                normals: GridLocation => Temperature): GridLocation => Temperature = {

    val predictTemperature = makeGrid(temperatures)

    def deviationTemperatures(location: GridLocation): Temperature = predictTemperature(location) - normals(location)

    deviationTemperatures
  }

}
