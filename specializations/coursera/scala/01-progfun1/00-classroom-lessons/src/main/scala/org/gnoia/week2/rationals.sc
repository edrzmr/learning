class Rational(n: Int, d: Int) {

  require(d != 0, "denominator must be nonzero")

  def this(n: Int) = this(n, 1)

  private def gcd(a: Int, b: Int): Int =
    if (b == 0) a
    else gcd(b, a % b)

  def numer = n

  def denom = d


  def + (that: Rational) =
    new Rational(
      numer * that.denom + that.numer * denom,
      denom * that.denom
    )

  def unary_- = new Rational(-numer, denom)

  def - (that: Rational) = this + -that

  def mul(that: Rational) = new Rational(
    numer * that.numer,
    denom * that.denom
  )

  def < (that: Rational) =
    numer * that.denom < that.numer * denom

  def max(that: Rational) =
    if (this < that) that
    else this

  override def toString: String = {
    val g = gcd(n, d)
    numer / g + "/" + denom / g
  }
}

object rationals {
  println("uhet")
}

val x = new Rational(1, 3)
val y = new Rational(5, 7)
val z = new Rational(3, 2)

x - y
x - y - z
y + y
x < y
x.max(y)
// new Rational(1, 0) // thrown exception
new Rational(2)
