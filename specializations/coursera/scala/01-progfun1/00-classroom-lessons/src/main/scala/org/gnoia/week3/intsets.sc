abstract class IntSet {

  def contains(x: Int): Boolean

  def incl(x: Int): IntSet

  def union(other: IntSet): IntSet
}

class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {

  def contains(x: Int): Boolean =
    if (x < elem) left contains x
    else if (x > elem) right contains x
    else true

  def incl(x: Int): IntSet =
    if (x < elem) new NonEmpty(elem, left incl x, right)
    else if (x > elem) new NonEmpty(elem, left, right incl x)
    else this

  override def toString: String = "{" + left + elem + right + "}"

  def union(other: IntSet): IntSet = {
    ((left union right) union other) incl (elem)
  }
}

object Empty extends IntSet {

  def contains(x: Int): Boolean = false

  def incl(x: Int): IntSet = new NonEmpty(x, Empty, Empty)

  override def toString: String = "."

  def union(other: IntSet): IntSet = other
}

println("Welcome to scala worksheet")
val t1 = new NonEmpty(3, Empty, Empty)
val t2 = t1 incl (4)

println(t1)
println(t2)

val t3 = new NonEmpty(10, Empty, Empty)
print(t3)

val t4 = t2 union t3
println(t4)
