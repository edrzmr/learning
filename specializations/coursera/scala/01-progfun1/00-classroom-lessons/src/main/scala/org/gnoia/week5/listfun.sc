object listfun {

  val nums = List(2, -4, 5, 7, 1)
  val fruits = List("abacaxi", "banana", "xuxu", "amora", "manga")

  nums.filter(_ > 0)
  nums.filterNot(_ > 0)
  nums.partition(_ > 0)

  nums.takeWhile(_ > 0)
  nums.dropWhile(_ > 0)
  nums.span(_ > 0)
}