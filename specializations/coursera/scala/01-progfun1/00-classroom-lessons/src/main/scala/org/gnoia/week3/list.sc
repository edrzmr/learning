import org.gnoia.week3.List
import org.gnoia.week3.Cons
import org.gnoia.week3.Nil


def count[T](xs: List[T]): Int =
  countAcc(xs, 0)


def countAcc[T](xs: List[T], acc: Int): Int =
  if (xs.isEmpty) acc
  else countAcc(xs.tail, acc + 1)


def nth[T](n: Int, xs: List[T]): T =
  if (xs.isEmpty) throw new IndexOutOfBoundsException("not found")
  else if (n == 0) xs.head
  else nth(n - 1, xs.tail)


val list = new Cons(1, new Cons(2, new Cons(69, new Nil)))

println(nth(2, list))
println(nth(0, list))
println(count(list))
