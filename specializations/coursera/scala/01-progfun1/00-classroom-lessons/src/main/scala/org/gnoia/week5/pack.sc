object pack {

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: _ => {
      val (a, b) = xs.span(_ == x)
      a :: pack(b)

    }
  }

  def encode1[T](list: List[T]): List[(T, Int)] = {

    def loop(x: List[List[T]], acc: List[(T, Int)]): List[(T, Int)] = x match {
      case Nil => acc.reverse
      case head :: tail => loop(tail, (head(0), head.length) :: acc)
    }

    loop(pack(list), Nil)
  }

  def encode[T](list: List[T]): List[(T, Int)] =
    pack(list).map(x => (x.head, x.length))


  pack(List("a", "a", "a", "b", "c", "c", "a"))
  encode1(List("a", "a", "a", "b", "c", "c", "a"))
  encode(List("a", "a", "a", "b", "c", "c", "a"))


  def mapFun[T, U](xs: List[T], f: T => U): List[U] =
    xs.foldRight(List[U]()) { (e, acc) => f(e) :: acc }

  mapFun[Int, Int](List(1, 2, 3), x => x + 1)

  def lengthFun[T](xs: List[T]): Int =
    xs.foldRight(0) { (_, count) => count + 1 }

  lengthFun(List(1, 2))
}