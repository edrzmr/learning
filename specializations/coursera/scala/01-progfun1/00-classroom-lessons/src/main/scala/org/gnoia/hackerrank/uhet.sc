object uhet {

  def split(s: String): (String, String) = (
    0.to(s.length - 1).by(2).toArray.foldLeft("")((a, b) => a + s(b)),
    1.to(s.length - 1).by(2).toArray.foldLeft("")((a, b) => a + s(b))
  )

  val s = split("Hacker")
  val x = "lala"
  x.is

  0.to(1 - 1).foreach(_ => {
    val (even, odd) = split("Hacker")
    println(even)
    println(odd)
  })


  val a = Array(1, 2, 3)
  val b = a.clone()
  a(0) = 69

  val c = a

  val m = Map.empty

  m.getOrElse()

  val m2 = m + "eder" -> 69


  val mr = 0.to(10).foldLeft(Map.empty[String, String])((m, _) => {
    val name = scala.io.StdIn.readLine.trim
    m + ("eder" -> i)
  })

}