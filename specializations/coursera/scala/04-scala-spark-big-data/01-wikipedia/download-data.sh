#!/usr/bin/env bash

set -eu

readonly DEBUG=${DEBUG:-"no"}
readonly baseUrl="http://alaska.epfl.ch/~dockermoocs/bigdata"
readonly destDir="src/main/resources/wikipedia"
readonly fileName="wikipedia.dat"
readonly file="${destDir}/${fileName}"
readonly url="${baseUrl}/${fileName}"

[[ ${DEBUG} != "no" ]] && set -x

if [[ -r ${file} ]]; then
  echo "* file already exists: ${file}"
  exit 1
fi

mkdir -p ${destDir}
curl "${url}" --output "${file}"