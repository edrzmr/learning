package wikipedia

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

import org.apache.spark.rdd.RDD

case class WikipediaArticle(title: String, text: String) {

  /**
    * @return Whether the text of this article mentions `lang` or not
    * @param lang Language to look for (e.g. "Scala")
    */
  def mentionsLanguage(lang: String): Boolean = text.split(' ').contains(lang)
}

object WikipediaRanking extends WikipediaRankingInterface {

  val langs = List("JavaScript",
                   "Java",
                   "PHP",
                   "Python",
                   "C#",
                   "C++",
                   "Ruby",
                   "CSS",
                   "Objective-C",
                   "Perl",
                   "Scala",
                   "Haskell",
                   "MATLAB",
                   "Clojure",
                   "Groovy")

  val conf: SparkConf  = new SparkConf().setMaster("local[2]").setAppName("Wikipedia")
  val sc: SparkContext = SparkContext.getOrCreate(conf)

  // Hint: use a combination of `sc.parallelize`, `WikipediaData.lines` and `WikipediaData.parse`
  val wikiRdd: RDD[WikipediaArticle] = sc
    .parallelize(WikipediaData.lines)
    .map(WikipediaData.parse)

  /** Returns the number of articles on which the language `lang` occurs.
    *  Hint1: consider using method `aggregate` on RDD[T].
    *  Hint2: consider using method `mentionsLanguage` on `WikipediaArticle`
    */
  def occurrencesOfLang(lang: String, rdd: RDD[WikipediaArticle]): Int = {
    def seqOP(acc: Int, article: WikipediaArticle): Int = if (article.mentionsLanguage(lang)) acc + 1 else acc
    def combOp(acc0: Int, acc1: Int): Int               = acc0 + acc1
    rdd.aggregate(0)(seqOP, combOp)
  }

  /* (1) Use `occurrencesOfLang` to compute the ranking of the languages
   *     (`val langs`) by determining the number of Wikipedia articles that
   *     mention each language at least once. Don't forget to sort the
   *     languages by their occurrence, in decreasing order!
   *
   *   Note: this operation is long-running. It can potentially run for
   *   several seconds.
   */
  def rankLangs(langs: List[String], rdd: RDD[WikipediaArticle]): List[(String, Int)] =
    langs
      .map(lang => (lang, occurrencesOfLang(lang, rdd)))
      .sortBy(_._2)(Ordering.Int.reverse)

  /* Compute an inverted index of the set of articles, mapping each language
   * to the Wikipedia pages in which it occurs.
   */
  def makeIndexOld(langs: List[String], rdd: RDD[WikipediaArticle]): RDD[(String, Iterable[WikipediaArticle])] = {

    def listByLang(lang: String): Iterable[WikipediaArticle] = {
      def seqOp(acc: List[WikipediaArticle], article: WikipediaArticle): List[WikipediaArticle] =
        if (article.mentionsLanguage(lang)) article :: acc else acc
      def combOp(acc0: List[WikipediaArticle], acc1: List[WikipediaArticle]): List[WikipediaArticle] = acc0 ++ acc1
      rdd.aggregate(List.empty[WikipediaArticle])(seqOp, combOp)
    }

    val index = langs.map(lang => (lang, listByLang(lang)))

    sc.parallelize(index)
  }

  def makeIndex(langs: List[String], rdd: RDD[WikipediaArticle]): RDD[(String, Iterable[WikipediaArticle])] =
    rdd.flatMap { article: WikipediaArticle =>
      langs.foldLeft(List.empty[(String, WikipediaArticle)]) {
        case (acc, lang) if !article.mentionsLanguage(lang) => acc
        case (acc, lang)                                    => (lang, article) :: acc
      }
    }.groupByKey()

  /* (2) Compute the language ranking again, but now using the inverted index. Can you notice
   *     a performance improvement?
   *
   *   Note: this operation is long-running. It can potentially run for
   *   several seconds.
   */

  def rankLangsUsingIndex(index: RDD[(String, Iterable[WikipediaArticle])]): List[(String, Int)] =
    index
      .mapValues(_.count(_ => true))
      .collect()
      .toList
      .sortBy(_._2)(Ordering.Int.reverse)

  /* (3) Use `reduceByKey` so that the computation of the index and the ranking are combined.
   *     Can you notice an improvement in performance compared to measuring *both* the computation of the index
   *     and the computation of the ranking? If so, can you think of a reason?
   *
   *   Note: this operation is long-running. It can potentially run for
   *   several seconds.
   */
  def rankLangsReduceByKey(langs: List[String], rdd: RDD[WikipediaArticle]): List[(String, Int)] =
    rdd.flatMap { article =>
      langs.map {
        case lang if article.mentionsLanguage(lang) => (lang, 1)
        case lang                                   => (lang, 0)
      }
    }.reduceByKey(_ + _)
      .collect()
      .toList
      .sortBy(_._2)(Ordering.Int.reverse)

  def main(args: Array[String]): Unit = {

    /* Languages ranked according to (1) */
    val langsRanked: List[(String, Int)] = timed("Part 1: naive ranking", rankLangs(langs, wikiRdd))

    /* An inverted index mapping languages to wikipedia pages on which they appear */
    def index: RDD[(String, Iterable[WikipediaArticle])] = makeIndex(langs, wikiRdd)

    /* Languages ranked according to (2), using the inverted index */
    val langsRanked2: List[(String, Int)] = timed("Part 2: ranking using inverted index", rankLangsUsingIndex(index))

    /* Languages ranked according to (3) */
    val langsRanked3: List[(String, Int)] =
      timed("Part 3: ranking using reduceByKey", rankLangsReduceByKey(langs, wikiRdd))

    /* Output the speed of each ranking */
    println(timing)
    sc.stop()
  }

  val timing = new StringBuffer

  def timed[T](label: String, code: => T): T = {
    val start  = System.currentTimeMillis()
    val result = code
    val stop   = System.currentTimeMillis()
    timing.append(s"Processing $label took ${stop - start} ms.\n")
    result
  }
}
