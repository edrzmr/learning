#!/usr/bin/python

import sys

hightest = -1.0
old_city = None

for line in sys.stdin:

    data = line.strip().split("\t")
    if len(data) != 2:
        continue

    city, value = data

    if old_city is not None and old_city != city:
        print old_city, "\t", hightest
        hightest = -1.0

    old_city = city
    value = float(value)
    hightest = hightest if hightest > value else value

if old_city != None:
    print old_city, "\t", hightest

