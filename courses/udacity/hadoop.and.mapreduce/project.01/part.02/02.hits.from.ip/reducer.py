#!/usr/bin/python

import sys

IP = '10.99.99.186'

count = 0

try:
    old_key, _ = sys.stdin.readline().strip().split('\t')
except:
    pass    

for line in sys.stdin:

    key, value = line.strip().split('\t')
    if key == IP:
        count += 1

print 'count\t{0}'.format(count)
