#!/usr/bin/python

import sys

RESOURCE = '/assets/js/the-associates.js'

try:
    old_key, _ = sys.stdin.readline().strip().split('\t')
except:
    sys.exit(0)    

count = 0

for line in sys.stdin:

    key, value = line.strip().split('\t')
    if key == RESOURCE:
        count += 1

print 'count\t{0}'.format(count)
