package com.lightbend.training.scalatrain

class JourneyPlanner(val trains: Set[Train]) {
  val stations: Set[Station] = trains.flatMap(_.stations)

  def trainsAt(station: Station): Set[Train] = trains.filter(_.stations.contains(station))

  def stopsAtWithForExpression(station: Station): Set[(Time, Train)] = for {
    train <- trains
    (time, candidateStation) <- train.schedule if candidateStation == station
  } yield (time, train)

  def stopsAtOld(station: Station): Set[(Time, Train)] =
    trains.flatMap { train =>
      train.schedule
        .filter(_._2 == station)
        .map(_._1 -> train)
    }

  def stopsAtOld2(station: Station): Set[(Time, Train)] = {
    trains.flatMap { train =>
      train
        .timeAt(station)
        .map(time => Set(time -> train))
        .getOrElse(Set.empty)
    }
  }

  /* magic */
  def stopsAt(station: Station): Set[(Time, Train)] = for {
    train: Train <- trains
    time: Time <- train.timeAt(station)
  } yield (time, train)

  def isShortTripWithoutPatternMatch(from: Station, to: Station): Boolean =
    trains.exists { train =>
      train.stations
        .dropWhile(_ != from)
        .slice(1, 3)
        .contains(to)
    }

  def isShortTrip(from: Station, to: Station): Boolean =
    trains.exists { train =>
      train.stations.dropWhile(_ != from) match {
        case `from` +: `to` +: _ => true
        case `from` +: _ +: `to` +: _ => true
        case _ => false
      }
    }

}
