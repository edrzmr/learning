package com.rockthejvm.zio.me

import zio.*
import com.rockthejvm.zio.utils.*

import java.io.{File, FileWriter}

object Fibers extends ZIOAppDefault {

  /**
   * Exercises
   */

  // 1 - zip two fibers without using the zip combinator
  // hint: create a fiber that waits for both
  def zipFibers[E, A, B](fiber1: Fiber[E, A], fiber2: Fiber[E, B]): ZIO[Any, Nothing, Fiber[E, (A, B)]] = {

    val resultEffect: IO[E, (A, B)] = for {
      a: A <- fiber1.join
      b: B <- fiber2.join
    } yield (a, b)

    val resultFiber: ZIO[Any, Nothing, Fiber[E, (A, B)]] = resultEffect.fork

    resultFiber
  }

  // 2 - same thing with orElse
  def chainFibers[E, A](fiber1: Fiber[E, A], fiber2: Fiber[E, A]): ZIO[Any, Nothing, Fiber[E, A]] =
    fiber1
      .join
      .foldZIO(
        failure = _ => fiber2.join,
        success = a => ZIO.succeed(a))
      .fork

  // 3 - distributing a task in between many fibers
  // spawn n fibers, count the n of words in each file,
  // then aggregate all the results together in one big number
  def generateRandomFile(path: String): Unit = {
    val random = scala.util.Random
    val chars = 'a' to 'z'
    val nWords = random.nextInt(2000) // at most 2000 random words

    val content = (1 to nWords)
      .map(_ => (1 to random.nextInt(10)).map(_ => chars(random.nextInt(26))).mkString) // one word for every 1 to nWords
      .mkString(" ")

    val writer = new FileWriter(new File(path))
    writer.write(content)
    writer.flush()
    writer.close()
  }

  val generateFilesProgram: ZIO[Any, Nothing, Unit] = ZIO.succeed {
    (1 to 10).foreach { index =>
      generateRandomFile(s"src/main/resources/testFile-$index.txt")
    }
  }

  // override def run: ZIO[Any, Nothing, Unit] = generateFilesProgram

  def countWorldsOnFile(path: String): ZIO[Any, Throwable, Int] = ZIO.attempt {
    val source = scala.io.Source.fromFile(path)

    val result = source
      .getLines()
      .map(line => line.split(" ").count(_.nonEmpty))
      .sum

    source.close()

    result
  }

  val countWords: ZIO[Any, Nothing, Int] = {
    val zioSeq: Seq[ZIO[Any, Throwable, Int]] = (1 to 10)
      .map(i => s"src/main/resources/testFile-$i.txt")
      .map(fn => countWorldsOnFile(fn))
      .map(zio => zio.fork)
      .map(zioFiber => zioFiber.flatMap(_.join))

    val result = zioSeq.reduce { (zioa, ziob) =>
      for {
        a <- zioa
        b <- ziob
        _ = println(s"a: $a, b: $b")
      } yield a + b
    }

    result.orDie
  }

  override def run: ZIO[Any, Nothing, Unit] = for {
    result <- countWords
    _ = println(s"total: $result")
  } yield ()
}
