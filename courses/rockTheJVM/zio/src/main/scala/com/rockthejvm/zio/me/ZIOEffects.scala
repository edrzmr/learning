package com.rockthejvm.zio.me

import zio._

object ZIOEffects {

  /**
   * Exercises
   *
   * 1 - sequence two ZIOs and take the value of the last one
   * 2 - sequence two ZIOs and take the value of the first one
   * 3 - run a ZIO forever
   * 4 - convert the value of a ZIO to something else
   * 5 - discard the value of a ZIO to Unit
   * 6 - recursion
   * 7 - fibonacci, hint: use ZIO.suspend/ZIO.suspendSucceed
   */

  // 1 - sequence two ZIOs and take the value of the last one
  def sequenceTakeLast[R, E, A, B](zioA: ZIO[R, E, A], zioB: ZIO[R, E, B]): ZIO[R, E, B] =
    for {
      _ <- zioA
      b <- zioB
    } yield b

  def sequenceTakeLast2[R, E, A, B](zioA: ZIO[R, E, A], zioB: ZIO[R, E, B]): ZIO[R, E, B] = zioA *> zioB

  // 2 - sequence two ZIOs and take the value of the first one
  def sequenceTakeFirst[R, E, A, B](zioA: ZIO[R, E, A], zioB: ZIO[R, E, B]): ZIO[R, E, A] =
    for {
      a <- zioA
      _ <- zioB
    } yield a

  def sequenceTakeFirst2[R, E, A, B](zioA: ZIO[R, E, A], zioB: ZIO[R, E, B]): ZIO[R, E, A] = zioA <* zioB

  // 3 - run a ZIO forever
  def runForever[R, E, A](zio: ZIO[R, E, A]): ZIO[R, E, A] = zio.flatMap(_ => runForever(zio))
  def runForever2[R, E, A](zio: ZIO[R, E, A]): ZIO[R, E, A] = zio *> runForever(zio)
  def runForever3[R, E, A](zio: ZIO[R, E, A]): ZIO[R, E, A] = {
    for {
      _ <- zio
    } yield ()
    runForever3(zio)
  }
  def runForever4[R, E, A](zio: ZIO[R, E, A]): ZIO[R, E, A] = {
    for {
      r <- runForever4(zio)
    } yield r
  }

  // 4 - convert the value of a ZIO to something else
  def convert[R, E, A, B](zio: ZIO[R, E, A], value: B): ZIO[R, E, B] =
    zio.map(_ => value)

  def convert2[R, E, A, B](zio: ZIO[R, E, A], value: B): ZIO[R, E, B] =
    zio.as(value)

  // 5 - discard the value of a ZIO to Unit
  def asUnit[R, E, A](zio: ZIO[R, E, A]): ZIO[R, E, Unit] = zio.map(_ => ())
  def asUnit2[R, E, A](zio: ZIO[R, E, A]): ZIO[R, E, Unit] = zio.unit

  // 6 - recursion
  object recursion {
    def sum(n: Int): Int =
      if (n <= 0) 0
      else n + sum(n - 1) // crash

    def sumZIO(n: Int): UIO[Int] =
      if (n <= 0) ZIO.succeed(0)
      else for {
        current <- ZIO.succeed(n)
        prevSum <- sumZIO(n - 1)
      } yield current + prevSum
  }

  // 7 - fibonacci, hint: use ZIO.suspend/ZIO.suspendSucceed
  object fibonacci {
    def fibo(n: Int): BigInt =
      if (n < 2) 1
      else fibo(n - 2) + fibo(n - 1)

    def fiboZIO(n: Int): UIO[BigInt] =
      if (n < 2) ZIO.succeed(1)
      else for {
          one <- ZIO.suspendSucceed(fiboZIO(n - 1))
          two <- fiboZIO(n - 2)
        } yield one + two
  }

  def main(args: Array[String]): Unit = {
    val runtime = Runtime.default
    given trace: Trace = Trace.empty

    Unsafe.unsafeCompat { (unsafe: Unsafe) =>
      given Unsafe = unsafe

      val firstEffect = ZIO.succeed {
        println("---> first effect")
        Thread.sleep(1000)
        1
      }

      val secondEffect = ZIO.succeed {
        println("---> second effect")
        Thread.sleep(2000)
        2
      }

      println(runtime.unsafe.run(sequenceTakeFirst(firstEffect, secondEffect)))
      println(runtime.unsafe.run(sequenceTakeLast(firstEffect, secondEffect)))
      println(runtime.unsafe.run(recursion.sumZIO(2000)))
    }
  }
}
