package com.rockthejvm.zio.me

import zio.*
import com.rockthejvm.zio.utils.*

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.Success
import scala.util.Failure

object AsynchonousEffects extends ZIOAppDefault {

  /** ***********
   * Exercises *
   * *********** */

  /**
   * 1 - lift a computation running on some (external) thread to a ZIO
   *
   * hint: invoke the cb when the computation is complete
   * hint 2: don't wrap the computation into a ZIO
   */
  def external2ZIO[A](computation: () => A)(executor: ExecutorService): Task[A] = {
    ZIO.async[Any, Throwable, A] { cb =>
      executor.execute { () =>
        try {
          cb(ZIO.succeed(computation()))
        } catch {
          case e: Throwable => cb(ZIO.fail(e))
        }
      }
    }
  }

  val demoExternal2ZIO: Task[Unit] = {

    val executor = Executors.newFixedThreadPool(8)

    val zio: Task[Int] = external2ZIO { () =>
      println(s"[${Thread.currentThread().getName}] computing the meaning of life on some thread")
      Thread.sleep(1000)
      42
    }(executor)

    zio.debugThread.unit
  }

  /**
   * 2 - lift a Future to a ZIO
   *
   * hint: invoke cb when the Future completes
   */
  def future2ZIO[A](future: => Future[A])(using ec: ExecutionContext): Task[A] = {
    ZIO.async { cb =>
      future.onComplete {
        case Success(value) => cb(ZIO.succeed(value))
        case Failure(exception) => cb(ZIO.fail(exception))
      }
    }
  }

  lazy val demoFuture2ZIO: Task[Unit] = {
    val executor = Executors.newFixedThreadPool(8)

    given ec: ExecutionContext = ExecutionContext.fromExecutorService(executor)

    val mol: Task[Int] = future2ZIO(Future {
      println(s"[${Thread.currentThread().getName}] computing the meaning of life on some thread")
      Thread.sleep(1000)
      42
    })

    mol.debugThread.unit
  }


  /**
   * 3 - implement a never-ending ZIO
   */
  def neverEndingZIO[A]: UIO[A] = ZIO.async { _ => () }

  // override def run = demoExternal2ZIO
  // override def run = demoFuture2ZIO
  override def run = neverEndingZIO
}
