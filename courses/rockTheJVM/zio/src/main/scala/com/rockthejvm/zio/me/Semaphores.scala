package com.rockthejvm.zio.me

import zio.*
import com.rockthejvm.zio.utils.*

object Semaphores extends ZIOAppDefault {

  def doWorkWhileLoggedIn(): UIO[Int] =
    ZIO.sleep(1.second) *> Random.nextIntBounded(100)

  /**
   * Exercise
   * 1. what is the code SUPPOSED to do?
   * 2. find if there's anything wrong
   * 3. fix the problem
   */
  val mySemaphore: UIO[Semaphore] = Semaphore.make(1) // a mutex
  val tasks: UIO[Seq[Int]] = ZIO.collectAllPar((1 to 10).map { id =>
    for {
      sem <- mySemaphore
      _ <- ZIO.succeed(s"[task $id] waiting to log in").debugThread
      res <- sem.withPermit {
        for {
          // critical section start
          _ <- ZIO.succeed(s"[task $id] logged in, working...").debugThread
          res <- doWorkWhileLoggedIn()
          _ <- ZIO.succeed(s"[task $id] done: $res").debugThread
        } yield res
      }
    } yield res
  })

  val tasksFixes = for {
    _ <- ZIO.succeed(">>> create semaphore").debugThread
    sem <- mySemaphore
    _ <- ZIO.succeed(s">>> semaphore created: $sem").debugThread


    outs <- ZIO.collectAllPar((1 to 10).map { id =>
      for {
        _ <- ZIO.succeed(s"[task $id waiting to log in]").debugThread
        workOut <- sem.withPermit {
          for {
            _ <- ZIO.succeed(s"[task $id] logged in, working...").debugThread
            workOut <- doWorkWhileLoggedIn()
            _ <- ZIO.succeed(s"[task $id] done: $workOut").debugThread
          } yield workOut
        }

      } yield workOut

    })
  } yield outs

  //override def run = tasks
  override def run = tasksFixes
}
