package com.rockthejvm.zio.me

import zio.*
import com.rockthejvm.zio.utils.*

import scala.collection.immutable.Queue


object Mutexes extends ZIOAppDefault {

  abstract class Mutex {
    def acquire: UIO[Unit]

    def release: UIO[Unit]
  }

  object Mutex {

    type Signal = Promise[Nothing, Unit]

    object Signal {
      def make: UIO[Promise[Nothing, Unit]] = Promise.make[Nothing, Unit]
    }

    case class State(locked: Boolean, waiting: Queue[Signal])

    val unlocked: State = State(locked = false, Queue())

    def make: UIO[Mutex] = Ref.make(unlocked).map { (stateRef: Ref[State]) =>
      new Mutex {
        /*
          Change the state of the Ref
          - if the mutex is unlocked, lock it
          - if the mutex is locked, state becomes (true, queue + new signal) and WAIT on that signal
         */
        override def acquire: UIO[Unit] = (for {
          signal <- Signal.make
          modiffyEffect <- stateRef.modify {
            case state@State(false, _) => ZIO.unit -> state.copy(locked = true)
            case state@State(true, waiting) =>
              signal.await -> state.copy(waiting = waiting.enqueue(signal))
          }
        } yield modiffyEffect)
          .flatten


        /*
          Change the state of the Ref
          - if the mutex is unlocked, leave the state unchanged
          - if the mutex is locked
            - if the queue is empty, unlock the mutex
            - if the queue is non-empty, take a signal out of the queue and complete it
         */
        override def release: UIO[Unit] = stateRef.modify {
          case state@State(false, _) => ZIO.unit -> state
          case state@State(true, waiting) if waiting.isEmpty => ZIO.unit -> state.copy(locked = false)
          case state@State(true, waiting) =>
            val (signal, nextQueue) = waiting.dequeue
            signal.succeed(()).unit -> state.copy(waiting = nextQueue)
        }.flatten
      }
    }
  }

  def workInCriticalRegion(): UIO[Int] =
    ZIO.sleep(1.second) *> Random.nextIntBounded(100)

  def demoNonLockingTasks(): UIO[Unit] =
    ZIO.collectAllParDiscard((1 to 10).toList.map { i =>
      for {
        _ <- ZIO.succeed(s"[task $i] working...").debugThread
        result <- workInCriticalRegion()
        _ <- ZIO.succeed(s"[task $i] got result: $result").debugThread
      } yield ()
    })

  def createTask(id: Int, mutex: Mutex): UIO[Int] = for {
    _ <- ZIO.succeed(s"[task $id] waiting for mutex...").debugThread

    _ <- mutex.acquire
    // critical region start
    _ <- ZIO.succeed(s"[task $id] mutex acquired, working...").debugThread
    result <- workInCriticalRegion().onInterrupt(mutex.release)
    _ <- ZIO.succeed(s"[task $id] got result: $result, releasing mutex").debugThread
    // critical region end
    _ <- mutex.release

  } yield result

  def demoLockingTasks(): UIO[Unit] = for {
    mutex <- Mutex.make
    _ <- ZIO.collectAllParDiscard((1 to 5).toList.map(i => createTask(i, mutex)))
  } yield ()


  // def run = demoNonLockingTasks()
  def run = demoLockingTasks()
}