/**
 * Memory of 16K registers, each 16 bit-wide. Out holds the value
 * stored at the memory location specified by address. If load==1, then 
 * the in value is loaded into the memory location specified by address 
 * (the loaded value will be emitted to out from the next time step onward).
 */

CHIP RAM16K
{
	IN in[16], load, address[14];
	OUT out[16];

	PARTS:
		/* split address[14] into two sub-buses, ah[2] and al[12] */
		And16(a[0..13] = address, b = true, out[0..11] = al, out[12..13] = ah);

		/* demux load signal */
		DMux4Way(in = load, sel = ah, a = l0, b = l1, c = l2, d = l3);

		/* access selected sub bank */
		RAM4K(in = in, out = o0, address = al, load = l0);
		RAM4K(in = in, out = o1, address = al, load = l1);
		RAM4K(in = in, out = o2, address = al, load = l2);
		RAM4K(in = in, out = o3, address = al, load = l3);

		/* mux output */
		Mux4Way16(out = out, sel = ah, a = o0, b = o1, c = o2, d = o3);
}
