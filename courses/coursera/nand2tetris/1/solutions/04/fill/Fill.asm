// Runs an infinite loop that listens to the keyboard input. 
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel. When no key is pressed, the
// program clears the screen, i.e. writes "white" in every pixel.

(INIT)
	@SCREEN
	D = A
	@ptr
	M = D     // ptr = SCREEN

	@n
	M = D
	@8192
	D = A
	@n
	M = D+M   // n = ptr + display size (512 * 256 = 8192)

(LOOP)
	@ptr
	D = M
	@n
	D = D-M
	@INIT
	D; JGE    // if (ptr >= n) goto INIT

	@READKEY
	0; JMP
(SETCOLOR)

	@color
	D = M
	@ptr
	A = M
	M = D    // *ptr = color

	D = A+1
	@ptr
	M = D    // ptr++

	@LOOP
	0; JMP   // goto LOOP

//----------------------------------------
(READKEY)
	@KBD
	D = M
	@WHITE
	D; JEQ    // if (key == 0) goto WHITE

	@color
	M = -1    // black
	@SETCOLOR
	0; JMP    // goto SETCOLOR

(WHITE)
	@color
	M = 0     // white
	@SETCOLOR
	0; JMP    // goto SETCOLOR
//----------------------------------------

