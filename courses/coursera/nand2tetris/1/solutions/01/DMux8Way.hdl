/**
 * 8-way demultiplexor:
 * {a, b, c, d, e, f, g, h} = {in, 0, 0, 0, 0, 0, 0, 0} if sel == 000
 *                            {0, in, 0, 0, 0, 0, 0, 0} if sel == 001
 *                            etc.
 *                            {0, 0, 0, 0, 0, 0, 0, in} if sel == 111
 */

CHIP DMux8Way
{
	IN in, sel[3];
	OUT a, b, c, d, e, f, g, h;

	PARTS:
		Not(in = sel[2], out = ns2);
		Not(in = sel[1], out = ns1);
		Not(in = sel[0], out = ns0);

		And(a = in, b = ns2, out = x);
		And(a = in, b = sel[2], out = y);

		And(a = ns1, b = ns0, out = r);
		And(a = ns1, b = sel[0], out = s);
		And(a = sel[1], b = ns0, out = t);
		And(a = sel[1], b = sel[0], out = u);

		And(a = x, b = r, out = a);
		And(a = x, b = s, out = b);
		And(a = x, b = t, out = c);
		And(a = x, b = u, out = d);
		And(a = y, b = r, out = e);
		And(a = y, b = s, out = f);
		And(a = y, b = t, out = g);
		And(a = y, b = u, out = h);
}
