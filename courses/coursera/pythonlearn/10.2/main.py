filename = raw_input('Ender file: ')
try:
    f = open(filename)
except:
    print 'cant open file: ' + filename

d = {}
for line in f:
    if not line.startswith('From '):
        continue

    line = line.rstrip()
    words = line.split()
    hour = words[5].split(':')[0]
    d[hour] = d.get(hour, 0) + 1

lst = d.items()
lst.sort()

for k, v in lst:
    print k, v
