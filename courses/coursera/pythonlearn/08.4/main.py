filename = raw_input('Ender file name: ')
try:
    f = open(filename)
except:
    print 'cant open file: ' + filename

lst = []
for line in f:
    for word in line.rstrip().split():
        if word not in lst:
            lst.append(word)

lst.sort()
print lst
