inp = raw_input('Enter score: ')
try:
	score = float(inp)
except:
	print 'Enter with numeric value'
	quit()

if score < 0.0:
	print 'Out of Range'
	quit()
if score > 1.0:
	print 'Out of Range'
	quit()

if score >= 0.9:
	grade = 'A'
elif score >= 0.8:
	grade = 'B'
elif score >= 0.7:
	grade = 'C'
elif score >= 0.6:
	grade = 'D'
elif score < 0.6:
	grade = 'F'

print grade
