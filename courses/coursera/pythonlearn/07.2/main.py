filename = raw_input('Ender file name: ')
try:
	file = open(filename)
except:
	print 'cant open file: ' + filename

count = 0
acm = 0

for line in file:
	if not line.startswith('X-DSPAM-Confidence: '):
		continue

	count += 1
	acm += float(line[line.index(':')+1:].strip())

print 'Average spam confidence: ' + str(acm/count)
