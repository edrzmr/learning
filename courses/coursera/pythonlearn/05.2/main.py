l = None
s = None

while True:
	cmd = raw_input('Enter a number: ')
	if cmd == 'done':
		break

	try:
		n = int(cmd)
	except:
		print 'Invalid input'
		continue

	if l is None:
		l = n
	elif l < n:
		l = n

	if s is None:
		s = n
	elif s > n:
		s = n

print 'Maximum is', l
print 'Minimum is', s
