Description
===========

2.2 Write a program that uses **raw\_input** to prompt a user for their name and
then welcomes them. Note that **raw\_input** will pop up a dialog box. Enter
**Sarah** in the pop-up box when you are prompted so your output will match the
desired output.
