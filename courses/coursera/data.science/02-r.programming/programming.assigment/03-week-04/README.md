Programming Assignment 3: Hospital Quality
==========================================

Introduction
============

This fourth programming assignment will be graded via a submit script which is
described below.

Detailed Instructions
=====================

Please download this document for detailed instructions about the assignment:

[Programming Assignment 3 Instructions](instructions.pdf)

Grading
=======

This assignment will be graded using unit tests executed via the submit script
that you run on your computer.

``` R
source("submitscript3.R")
```

The first time you run the submit script it will prompt you for your Submission
login and Submission password. These can be found at the top of the Programming
Assignments page. To execute the submit script, type

``` R
submit()
```

at the console prompt (after source-ing the file). NOTE that the submit script
requires that you be connected to the Internet in order to work properly. When
you execute the submit script in R, you will see the following menu (after
typing in your submission login email and password):

``` R
[1] 'best' part 1
[2] 'best' part 2
[3] 'best' part 3
[4] 'rankhospital' part 1
[5] 'rankhospital' part 2
[6] 'rankhospital' part 3
[7] 'rankhospital' part 4
[8] 'rankall' part 1
[9] 'rankall' part 2
[10] 'rankall' part 3
Which part are you submitting [1-10]?
```

Entering a number between 1 and 10 will execute the corresponding part of the
homework. We will compare the output of your functions to the correct output.
For each test passed you receive the specified number of points on the
Assignments List web page. 

You are finished when you have successfully submitted everything using submit()
and you see scores on the assignment page. You can ignore the Submit buttons to
the right of each score. They are only to be used when firewall or proxy
settings prevent users from successfully using the submit() script. The submit()
script will describe how to create files for uploading if there are problems,
but under normal circumstances, there is NO NEED to use the Submit buttons on
the assignment page.
