## These functions implement inverse matrix that supports cache

## Create a matrix 'object' to store raw matrix and cache his inverse

makeCacheMatrix <- function(x = matrix())
{
        i <- NULL

        set <- function(mat) {
                i <<- NULL
                x <<- mat
        }

        get <- function() x

        setInv = function(inv) {
                i <<- inv
        }

        getInv = function() i

        list(set = set, get = get, setInv = setInv, getInv = getInv)
}


## If there are a cached inverse matrix, return it, otherwise compute, cache,
## and return it

cacheSolve <- function(x, ...)
{
        inv <- x$getInv()
        if (!is.null(inv)) {
                message('getting cached data')
                return(inv)
        }

        m <- x$get()
        inv <- solve(m, ...)
        x$setInv(inv)
        return(inv)
}
