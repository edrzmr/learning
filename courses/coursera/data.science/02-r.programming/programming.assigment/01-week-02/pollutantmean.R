pollutantmean <- function(directory, pollutant, id = 1:332)
{
        v <- vector()
        for (i in id) {
                filename <- paste0(directory, '/', sprintf('%03d', i), '.csv')
                v <- rbind(v, read.csv(filename))
        }
        
        round(mean(v[[pollutant]], na.rm = TRUE), 3)
}
