1. What is produced at the end of this snippet of R code?
 ``` R
 set.seed(1)
 rpois(5, 2)
 ```
 - [x] A vector with the numbers 1, 1, 2, 4, 1
  * Explanation: Because the `set.seed()' function is used, 'rpois()' will
    always output the same vector in this code.
 - [ ] A vector with the numbers 3.3, 2.5, 0.5, 1.1, 1.7
 - [ ] A vector with the numbers 1, 4, 1, 1, 5
 - [ ] It is impossible to tell because the result is random

2. What R function can be used to generate standard Normal random variables?
 - [ ] qnorm
 - [x] rnorm
  * Explanation: Functions beginning with the `r' prefix are used to simulate
    random variates. Standard probability distributions in R have a set of four
    functions that can be used to simulate variates, evaluate the density,
    evaluate the cumulative density, and evaluate the quantile function.
 - [ ] dnorm
 - [ ] pnorm

3. When simulating data, why is using the set.seed() function important?
 - [x] It ensures that the sequence of random numbers starts in a specific place
       and is therefore reproducible.
 - [ ] It ensures that the sequence of random numbers is truly random.
 - [ ] It ensures that the random numbers generated are within specified
       boundaries.
 - [ ] It can be used to generate non-uniform random numbers.

4. Which function can be used to evaluate the inverse cumulative distribution
function for the Poisson distribution?
 - [x] qpois
  * Explanation: Probability distribution functions beginning with the 'q'
    prefix are used to evaluate the quantile (inverse cumulative distribution)
    function.
 - [ ] dpois
 - [ ] rpois
 - [ ] ppois

5. What does the following code do?
 ``` R
 set.seed(10)
 x <- rep(0:1, each = 5)
 e <- rnorm(10, 0, 20)
 y <- 0.5 + 2 * x + e
 ```
 - [ ] Generate uniformly distributed random data
 - [x] Generate data from a Normal linear modelCorrect1.00
 - [ ] Generate data from a Poisson generalized linear model
 - [ ] Generate random exponentially distributed data

6. What R function can be used to generate Binomial random variables?
 - [ ] pbinom
 - [ ] dbinom
 - [ ] qbinom
 - [x] rbinom

7. What aspect of the R runtime does the profiler keep track of when an R
expression is evaluated?
 - [ ] the global environment
 - [ ] the package search list
 - [ ] the working directory
 - [x] the function call stackCorrect1.00

8. Consider the following R code
 ``` R
 library(datasets)
 Rprof()
 fit <- lm(y ~ x1 + x2)
 Rprof(NULL)
 ```
 (Assume that y, x1, and x2 are present in the workspace.) Without running the
 code, what percentage of the run time is spent in the 'lm' function, based on
 the 'by.total' method of normalization shown in 'summaryRprof()'?
 - [ ] 23%
 - [ ] 50%
 - [ ] It is not possible to tell
 - [x] 100%
  * Explanation: When using `by.total' normalization, the top-level function (in
    this case, 'lm()') always takes 100% of the time.

9. When using 'system.time()', what is the user time?
 - [ ] It is a measure of network latency
 - [ ] It is the time spent by the CPU waiting for other tasks to finish
 - [ ] It is the "wall-clock" time it takes to evaluate an expression
 - [x] It is the time spent by the CPU evaluating an expression

10. If a computer has more than one available processor and R is able to take
advantage of that, then which of the following is true when using
'system.time()'?
 - [ ] user time is 0
 - [x] elapsed time may be smaller than user time
 - [ ] user time is always smaller than elapsed time
 - [ ] elapsed time is 0
