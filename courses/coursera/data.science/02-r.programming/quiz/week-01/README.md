Introduction
============

This first quiz will check your ability to execute basic operations on objects
in R and to understand some basic concepts. For questions 11–20 you will need to
load a dataset into R and do some basic manipulations in order to answer the
questions on the quiz.

You may want to print a copy of the quiz questions to look at as you work on the
assignment. It is recommended that you save your answers as you go in the event
that a technical problem should occur with your network connection or computer.
Ultimately, you must submit the quiz online to get credit!

Data
====

The zip file containing the data for questions 11–20 in this Quiz can be
downloaded here:

[Week 1 Quiz Data](hw1_data.zip)

For this assignment you will need to unzip this file in your working directory.

Questions
=========

1. R was developed by statisticians working at

 - [x] The University of Auckland
  * Explanation: The R language was developed by Ross Ihaka and Robert Gentleman
    who were statisticians at the University of Auckland in New Zealand.
 - [ ] StatSci
 - [ ] Insightful
 - [ ] Microsoft

2. The definition of free software consists of four freedoms (freedoms 0
through 3). Which of the following is NOT one of the freedoms that are part of
the definition?
 - [ ] The freedom to run the program, for any purpose.
 - [ ] The freedom to improve the program, and release your improvements to the
       public, so that the whole community benefits.
 - [x] The freedom to sell the software for any price.
  * Explanation: This is not part of the free software definition. The free
    software definition does not mention anything about selling software
    (although it does not disallow it).
 - [ ] The freedom to redistribute copies so you can help your neighbor.

3. In R the following are all atomic data types EXCEPT
 - [ ] integer
 - [ ] numeric
 - [x] table
  * Explanation: 'table' is not an atomic data type in R.
 - [ ] logical

4. If I execute the expression x <\- 4L in R, what is the class of the object
'x' as determined by the `class()' function?
 - [ ] matrix
 - [ ] numeric
 - [x] integer
  * Explanation: The 'L' suffix creates an integer vector as opposed to a
    numeric vector.
 - [ ] character

5. What is the class of the object defined by the expression
x <\- c(4, "a", TRUE)?
 - [x] character
  * Explanation: The character class is the "lowest common denominator" here and
    so all elements will be coerced into that class. R does automatic coercion
    of vectors so that all elements of the vector are the same data class.
 - [ ] logical
 - [ ] mixed
 - [ ] integer

6. If I have two vectors x <\- c(1,3, 5) and y <\- c(3, 2, 10), what is produced
by the expression cbind(x, y)?
 - [ ] a vector of length 2
 - [ ] a 2 by 2 matrix
 - [x] a 3 by 2 numeric matrix
  * Explanation: The 'cbind' function treats vectors as if they were columns of
    a matrix. It then takes those vectors and binds them together column-wise to
    create a matrix.
 - [ ] a 2 by 3 matrix

7. A key property of vectors in R is that
 - [x] elements of a vector all must be of the same class
 - [ ] elements of a vector can only be character or numeric
 - [ ] elements of a vector can be of different classes
 - [ ] the length of a vector must be less than 32,768

8. Suppose I have a list defined as x <\- list(2, "a", "b", TRUE). What does
x[[1]] give me?
 - [ ] a list containing the number 2.
 - [ ] a list containing a numeric vector of length 1.
 - [ ] a character vector containing the element "2".
 - [x] a numeric vector containing the element 2.

9. Suppose I have a vector x <\- 1:4 and a vector y <\- 2. What is produced by
the expression x + y?
 - [x] a numeric vector with elements 3, 4, 5, 6.
 - [ ] a numeric vector with elements 1, 2, 3, 6.
 - [ ] an integer vector with elements 3, 2, 3, 6.
 - [ ] a numeric vector with elements 3, 2, 3, 4.

10. Suppose I have a vector x <\- c(17, 14, 4, 5, 13, 12, 10) and I want to set
all elements of this vector that are greater than 10 to be equal to 4. What R
code achieves this?
 - [ ] x[x == 4] > 10
 - [ ] x[x < 10] <\- 4
 - [x] x[x >= 11] <\- 4
  * Explanation: You can create a logical vector with the expression x >= 11 and
    then use the [ operator to subset the original vector x.
 - [ ] x[x > 4] <\- 10

11. In the dataset provided for this Quiz, what are the column names of the
dataset?
 - [ ] Month, Day, Temp, Wind
 - [x] Ozone, Solar.R, Wind, Temp, Month, Day
  * Explanation: You can get the column names of a data frame with the 'names()'
    function.
 - [ ] 1, 2, 3, 4, 5, 6
 - [ ] Ozone, Solar.R, Wind

12. Extract the first 2 rows of the data frame and print them to the console.
What does the output look like?
 - [x] 
 ```
   Ozone Solar.R Wind Temp Month Day
 1    41     190  7.4   67     5   1
 2    36     118  8.0   72     5   2
 ```
 - [ ] 
 ```
   Ozone Solar.R Wind Temp Month Day
 1    18     224 13.8   67     9  17
 2    NA     258  9.7   81     7  22
 ```
 - [ ] 
 ```
   Ozone Solar.R Wind Temp Month Day
 1     9      24 10.9   71     9  14
 2    18     131  8.0   76     9  29
 ```
 - [ ] 
 ```
   Ozone Solar.R Wind Temp Month Day
 1     7      NA  6.9   74     5  11
 2    35     274 10.3   82     7  17
 ```

13. How many observations (i.e. rows) are in this data frame?
 - [ ] 45
 - [x] 153
  * Explanation: You can use the `nrows()' function to compute the number of
    rows in a data frame.
 - [ ] 160
 - [ ] 129

14. Extract the last 2 rows of the data frame and print them to the console.
What does the output look like?
 - [ ] 
 ```
     Ozone Solar.R Wind Temp Month Day
 152    31     244 10.9   78     8  19
 153    29     127  9.7   82     6   7
 ```
 - [x] Explanation: The `tail()' function is an easy way to extract the last few
       elements of an R object.
 ```
     Ozone Solar.R Wind Temp Month Day
 152    18     131  8.0   76     9  29
 153    20     223 11.5   68     9  30
 ```
 - [ ] 
 ```
     Ozone Solar.R Wind Temp Month Day
 152    11      44  9.7   62     5  20
 153   108     223  8.0   85     7  25
 ```
 - [ ] 
 ```
     Ozone Solar.R Wind Temp Month Day
 152    34     307 12.0   66     5  17
 153    13      27 10.3   76     9  18
 ```

15. What is the value of Ozone in the 47th row?
 - [ ] 18
 - [ ] 34
 - [ ] 63
 - [x] 21
  * Explanation: The single bracket [ operator can be used to extract individual
    rows of a data frame.

16. How many missing values are in the Ozone column of this data frame?
 - [ ] 43
 - [x] 37
  * Explanation: The 'is.na' function can be used to test for missing values.
 - [ ] 9
 - [ ] 78

17. What is the mean of the Ozone column in this dataset? Exclude missing values
(coded as NA) from this calculation.
 - [x] 42.1
  * Explanation: The `mean' function can be used to calculate the mean.
 - [ ] 31.5
 - [ ] 18.0
 - [ ] 53.2

18. Extract the subset of rows of the data frame where Ozone values are above 31
and Temp values are above 90. What is the mean of Solar.R in this subset?
 - [ ] 185.9
 - [x] 212.8
  * Explanation: You need to construct a logical vector in R to match the
    question's requirements. Then use that logical vector to subset the data
    frame.
 - [ ] 334.0
 - [ ] 205.0

19. What is the mean of "Temp" when "Month" is equal to 6?
 - [ ] 90.2
 - [ ] 85.6
 - [x] 79.1
 - [ ] 75.3

20. What was the maximum ozone value in the month of May (i.e. Month = 5)?
 - [ ] 100
 - [x] 115
 - [ ] 97
 - [ ] 18
