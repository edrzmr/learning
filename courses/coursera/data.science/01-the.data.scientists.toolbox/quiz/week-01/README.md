1. Which of the following are courses in the Data Science Specialization? Select
all that apply.

- [x] Statistical Inference
- [ ] Machine Learning for Hackers
- [x] Regression Models
- [ ] Python Programming

2. Why are we using R for the course track? Select all that apply.

- [x] R is free
- [x] R has a nice IDE, Rstudio
- [ ] R is a general purpose programming language
- [ ] R is the only programming language data scientists use

3. What are good ways to find answers to questions in this course track? Select
all that apply.

- [x] Searching Google
- [x] Looking through R help files
- [ ] Emailing your instructors
- [ ] Emailing the community TAs

4. What are characteristics of good questions on the message boards? Select all
that apply.

- [x] Includes follow-ups if you answer your own question
- [x] Is very specific
- [ ] Is insulting or rude
- [ ] Asks for a code fix without explanation

5. Which of the following packages provides machine learning functionality?
Select all that apply

- [x] kernlab
- [ ] filehash
- [ ] lubridate
- [x] gbm
