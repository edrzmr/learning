Getting and Cleaning Data Project
=================================

This project serves to demonstrate the collection and cleaning of a tidy data
set that can be used for subsequent analysis.

For more information look at [CodeBook](CodeBook.md)
