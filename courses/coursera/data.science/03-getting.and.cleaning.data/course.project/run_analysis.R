# /usr/bin/env r

library(plyr)

rm(list=ls())

fileName <- "getdata_dataset.zip"
fileUrl <- "https://d396qusza40orc.cloudfront.net/getdata%2Fprojectfiles%2FUCI%20HAR%20Dataset.zip"
dataDir <- "UCI HAR Dataset"

# download data file
if (!file.exists(fileName))
{
        download.file(fileUrl, fileName, method="curl")
}

# extract data file
if (!file.exists(dataDir))
{
        unzip(fileName) 
}

feature <- read.table(paste0(dataDir, '/features.txt'))
featureId <- grep('.*mean.*|.*std.*', feature[, 2])
featureNames <- as.character(feature[featureId, 2])
featureNames <- gsub('[()]', '', featureNames) 
featureNames <- gsub('std', 'Std', featureNames) 
featureNames <- gsub('mean', 'Mean', featureNames) 
featureNames <- gsub('-', '', featureNames) 

activity <- read.table(paste0(dataDir, '/activity_labels.txt'))
activity[,2] <- as.character(activity[, 2])

# load train data
trainSubject <- read.table(paste0(dataDir, '/train/subject_train.txt'))
trainY <- read.table(paste0(dataDir, '/train/y_train.txt'))
trainX <- read.table(paste0(dataDir, '/train/X_train.txt'))[featureId]

# fix train colnames
colnames(trainSubject) <- 'subject'
colnames(trainY) <- 'activity'
colnames(trainX) <- featureNames

# merge train data
train <- cbind(trainSubject, trainY, trainX)
rm(list = c("trainSubject", "trainY", "trainX"))

# load test data
testSubject <- read.table(paste0(dataDir, '/test/subject_test.txt'))
testY <- read.table(paste0(dataDir, '/test/y_test.txt'))
testX <- read.table(paste0(dataDir, '/test/X_test.txt'))[featureId]

# fix test colnames
colnames(testSubject) <- 'subject'
colnames(testY) <- 'activity'
colnames(testX) <- featureNames

# merge test data
test <- cbind(testSubject, testY, testX)
rm(list = c("testSubject", "testY", "testX"))

rm(list = c("feature", "featureId", "featureNames"))

tidy = rbind(train, test)
rm(list = c("train", "test"))

tidy$activity = factor(tidy$activity, levels = activity[, 1], labels = activity[, 2])
tidy$subject = as.factor(tidy$subject)
rm(activity)

tidy <- ddply(tidy, .(subject, activity), function(x) colMeans(x[, 3:81]))
write.table(tidy, "tidy.txt", row.name=FALSE)
