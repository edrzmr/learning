import simplegui
import random
import math

secret_number = None
secret_range = 100
count = 0


def update_count():
    global count
    count = int(math.ceil(math.log(secret_range+1, 2)))


def new_game():
    global secret_number
    global count

    count = int(math.ceil(math.log(secret_range+1, 2)))
    secret_number = random.randrange(0, secret_range)

    print 'New game. Range is from to', secret_range
    print 'Number of remaining guesses is', count
    print


def range100():
    global secret_range
    secret_range = 100
    new_game()


def range1000():
    global secret_range
    secret_range = 1000
    new_game()


def input_guess(guess):
    global count

    if count == 0:
        print 'You loose, try again!'
        print
        new_game()
        return

    guess = int(guess)
    print 'Guess was', guess
    if guess == secret_number:
        print 'Corrent!'
        print
        print 'You win, try again!'
        print
        new_game()
        return

    if guess < secret_number:
        print 'Highter!'
    else:
        print 'Lower!'

    count -= 1
    print 'Number of remaining guesses is', count
    print


f = simplegui.create_frame('guess the number', 200, 200)
f.add_button('Range is [0,100)', range100, 150)
f.add_button('Range is [0,1000)', range1000, 150)
f.add_input('Enter a guess:', input_guess, 50)
f.start()
new_game()
