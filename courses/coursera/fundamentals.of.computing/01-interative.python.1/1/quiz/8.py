from math import tan
from math import pi

def area(number_of_sides, length):

    return 1/4.0 * number_of_sides * length**2 / tan(pi/number_of_sides)

print area(7, 3)
