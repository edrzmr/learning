# Question 8
# 
# We will again use the BankAccount class from the previous problem. You should
# be able to use the same definition for both problems.
# 
# Of course, a bank with only one account will go out of business, so we want
# our BankAccount class to work correctly with many accounts. Naturally, each
# bank account should have its own balance, with deposits and withdrawals going
# to the appropriate account. Similarly, the penalty fees for each account
# should be kept separate.
# 
# class BankAccount:
#     def __init__(self, initial_balance):
#         """Creates an account with the given balance."""
#         ...
#     def deposit(self, amount):
#         """Deposits the amount into the account."""
#         ...
#     def withdraw(self, amount):
#         """
#         Withdraws the amount from the account.  Each withdrawal resulting in a
#         negative balance also deducts a penalty fee of 5 dollars from the balance.
#         """
#         ...
#     def get_balance(self):
#         """Returns the current balance in the account."""
#         ...
#     def get_fees(self):
#         """Returns the total fees ever deducted from the account."""
#         ...
# Here's one possible test with multiple accounts. It should print the values
# 10, 5, 5, and 0.
# 
# account1 = BankAccount(10)
# account1.withdraw(15)
# account2 = BankAccount(15)
# account2.deposit(10)
# account1.deposit(20)
# account2.withdraw(20)
# print account1.get_balance(), account1.get_fees(), account2.get_balance(), account2.get_fees()
#
# Copy-and-paste the following much longer test. What four numbers are printed
# at the end? Enter the four numbers, separated only by spaces.
# 
# account1 = BankAccount(20)
# account1.deposit(10)
# account2 = BankAccount(10)
# account2.deposit(10)
# account2.withdraw(50)
# account1.withdraw(15)
# account1.withdraw(10)
# account2.deposit(30)
# account2.withdraw(15)
# account1.deposit(5)
# account1.withdraw(10)
# account2.withdraw(10)
# account2.deposit(25)
# account2.withdraw(15)
# account1.deposit(10)
# account1.withdraw(50)
# account2.deposit(25)
# account2.deposit(25)
# account1.deposit(30)
# account2.deposit(10)
# account1.withdraw(15)
# account2.withdraw(10)
# account1.withdraw(10)
# account2.deposit(15)
# account2.deposit(10)
# account2.withdraw(15)
# account1.deposit(15)
# account1.withdraw(20)
# account2.withdraw(10)
# account2.deposit(5)
# account2.withdraw(10)
# account1.deposit(10)
# account1.deposit(20)
# account2.withdraw(10)
# account2.deposit(5)
# account1.withdraw(15)
# account1.withdraw(20)
# account1.deposit(5)
# account2.deposit(10)
# account2.deposit(15)
# account2.deposit(20)
# account1.withdraw(15)
# account2.deposit(10)
# account1.deposit(25)
# account1.deposit(15)
# account1.deposit(10)
# account1.withdraw(10)
# account1.deposit(10)
# account2.deposit(20)
# account2.withdraw(15)
# account1.withdraw(20)
# account1.deposit(5)
# account1.deposit(10)
# account2.withdraw(20)
# print account1.get_balance(), account1.get_fees(), account2.get_balance(), account2.get_fees()

class BankAccount:
    def __init__(self, initial_balance):
        self.balance = initial_balance
        self.fees = 0

    def deposit(self, amount):
        self.balance += amount

    def withdraw(self, amount):
        self.balance -= amount
        if self.balance < 0:
                self.balance -= 5
                self.fees += 5

    def get_balance(self):
        return self.balance

    def get_fees(self):
        return self.fees

def test():
        account1 = BankAccount(10)
        account1.withdraw(15)
        account1.deposit(20)
        account2 = BankAccount(15)
        account2.deposit(10)
        account2.withdraw(20)
        print account1.get_balance(), account1.get_fees(), account2.get_balance(), account2.get_fees()

def resp():
        account1 = BankAccount(20)
        account1.deposit(10)
        account2 = BankAccount(10)
        account2.deposit(10)
        account2.withdraw(50)
        account1.withdraw(15)
        account1.withdraw(10)
        account2.deposit(30)
        account2.withdraw(15)
        account1.deposit(5)
        account1.withdraw(10)
        account2.withdraw(10)
        account2.deposit(25)
        account2.withdraw(15)
        account1.deposit(10)
        account1.withdraw(50)
        account2.deposit(25)
        account2.deposit(25)
        account1.deposit(30)
        account2.deposit(10)
        account1.withdraw(15)
        account2.withdraw(10)
        account1.withdraw(10)
        account2.deposit(15)
        account2.deposit(10)
        account2.withdraw(15)
        account1.deposit(15)
        account1.withdraw(20)
        account2.withdraw(10)
        account2.deposit(5)
        account2.withdraw(10)
        account1.deposit(10)
        account1.deposit(20)
        account2.withdraw(10)
        account2.deposit(5)
        account1.withdraw(15)
        account1.withdraw(20)
        account1.deposit(5)
        account2.deposit(10)
        account2.deposit(15)
        account2.deposit(20)
        account1.withdraw(15)
        account2.deposit(10)
        account1.deposit(25)
        account1.deposit(15)
        account1.deposit(10)
        account1.withdraw(10)
        account1.deposit(10)
        account2.deposit(20)
        account2.withdraw(15)
        account1.withdraw(20)
        account1.deposit(5)
        account1.deposit(10)
        account2.withdraw(20)
        print account1.get_balance(), account1.get_fees(), account2.get_balance(), account2.get_fees()
        # -55 45 45 20

resp()
