# Question 2
#
# What is the position of the center of the bottom-right card (King of Diamonds)
# in the tiled image discussed in the "Tiled images" video? Again, remember that
# each card in this tiled image has size 73 x 98 pixels.
#
# Enter two numbers, separated only by spaces.

CARD_SIZE = (73, 98)
CARD_CENTER = (CARD_SIZE[0] / 2.0, CARD_SIZE[1] / 2.0)
RANKS = ('A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K')
SUITS = ('C', 'S', 'H', 'D')

def calc_center_pos(rank, suit):
    i = RANKS.index(rank)
    j = SUITS.index(suit)
    return (CARD_CENTER[0] + i * CARD_SIZE[0], CARD_CENTER[1] + j * CARD_SIZE[1])

print calc_center_pos('K', 'D')
