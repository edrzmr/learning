# program template for Spaceship
import simplegui
import math
import random

# globals for user interface
WIDTH = 800
HEIGHT = 600
score = 0
lives = 3
time = 0

class ImageInfo:
    def __init__(self, center, size, radius = 0, lifespan = None, animated = False):
        self.center = center
        self.size = size
        self.radius = radius
        if lifespan:
            self.lifespan = lifespan
        else:
            self.lifespan = float('inf')
        self.animated = animated

    def get_center(self):
        return self.center

    def get_size(self):
        return self.size

    def get_radius(self):
        return self.radius

    def get_lifespan(self):
        return self.lifespan

    def get_animated(self):
        return self.animated

# art assets created by Kim Lathrop, may be freely re-used in non-commercial projects, please credit Kim

# debris images - debris1_brown.png, debris2_brown.png, debris3_brown.png, debris4_brown.png
#                 debris1_blue.png, debris2_blue.png, debris3_blue.png, debris4_blue.png, debris_blend.png
debris_info = ImageInfo([320, 240], [640, 480])
debris_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/debris2_blue.png")

# nebula images - nebula_brown.png, nebula_blue.png
nebula_info = ImageInfo([400, 300], [800, 600])
nebula_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/nebula_blue.f2014.png")

# splash image
splash_info = ImageInfo([200, 150], [400, 300])
splash_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/splash.png")

# ship image
ship_info = ImageInfo([45, 45], [90, 90], 35)
ship_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/double_ship.png")

# missile image - shot1.png, shot2.png, shot3.png
missile_info = ImageInfo([5,5], [10, 10], 3, 50)
missile_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/shot2.png")

# asteroid images - asteroid_blue.png, asteroid_brown.png, asteroid_blend.png
asteroid_info = ImageInfo([45, 45], [90, 90], 40)
asteroid_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/asteroid_blue.png")

# animated explosion - explosion_orange.png, explosion_blue.png, explosion_blue2.png, explosion_alpha.png
explosion_info = ImageInfo([64, 64], [128, 128], 17, 24, True)
explosion_image = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/explosion_alpha.png")

# sound assets purchased from sounddogs.com, please do not redistribute
soundtrack = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/soundtrack.mp3")
missile_sound = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/missile.mp3")
missile_sound.set_volume(.5)
ship_thrust_sound = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/thrust.mp3")
explosion_sound = simplegui.load_sound("http://commondatastorage.googleapis.com/codeskulptor-assets/sounddogs/explosion.mp3")

# helper functions to handle transformations
def angle_to_vector(ang):
    return [math.cos(ang), math.sin(ang)]

def dist(p,q):
    return math.sqrt((p[0] - q[0]) ** 2+(p[1] - q[1]) ** 2)


# Ship class
class Ship:
    def __init__(self, pos, vel, angle, image, info):
        self.pos = [pos[0], pos[1]]
        self.vel = [vel[0], vel[1]]
        self.thrust = False
        self.angle = angle
        self.angle_vel = 0
        self.image = image
        self.image_center = info.get_center()
        self.image_size = info.get_size()
        self.radius = info.get_radius()

    def increment_angle_vel(self, n):
        self.angle_vel += n

    def decrement_angle_vel(self, n):
        self.angle_vel -= n

    def enable_thrusters(self, enable):
        self.thrust = enable

        if False == enable:
            ship_thrust_sound.pause()
            ship_thrust_sound.rewind()
            return

        ship_thrust_sound.play()

    def shoot(self):
        global a_missile

        ang = 0
        ang_vel = 0
        img = missile_image
        info = missile_info
        sound = missile_sound

        forward = angle_to_vector(self.angle)

        pos = [self.pos[0] + self.radius * forward[0], self.pos[1] + self.radius * forward[1]]
        vel = [(forward[0] + self.vel[0]) * 2, (forward[1] + self.vel[1]) * 2]

        a_missile = Sprite(pos, vel, ang, ang_vel, img, info, sound)

    def draw(self,canvas):
        img = self.image
        center = list(self.image_center)
        center[0] = (self.image_center[0], self.image_center[0] + self.image_size[0])[self.thrust]
        size = self.image_size
        pos = self.pos
        angle = self.angle
        canvas.draw_image(img, center, size, pos, size, angle)

    def update(self):
        # update position
        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]
        self.angle += self.angle_vel

        # handle acceleration
        if True ==  self.thrust:
            forward = angle_to_vector(self.angle)
            self.vel[0] += 0.1 * forward[0]
            self.vel[1] += 0.1 * forward[1]

        # handle friction
        self.vel[0] *= 0.98
        self.vel[1] *= 0.98

        # wrap arround the screen
        self.pos[0] = self.pos[0] % WIDTH
        self.pos[1] = self.pos[1] % HEIGHT


# Sprite class
class Sprite:
    def __init__(self, pos, vel, ang, ang_vel, image, info, sound = None):
        self.pos = [pos[0], pos[1]]
        self.vel = [vel[0], vel[1]]
        self.angle = ang
        self.angle_vel = ang_vel
        self.image = image
        self.image_center = info.get_center()
        self.image_size = info.get_size()
        self.radius = info.get_radius()
        self.lifespan = info.get_lifespan()
        self.animated = info.get_animated()
        self.age = 0
        if sound:
            sound.rewind()
            sound.play()

    def draw(self, canvas):
        img = self.image
        center = self.image_center
        size = self.image_size
        pos = self.pos
        angle = self.angle
        canvas.draw_image(img, center, size, pos, size, angle)

    def update(self):
        # update position
        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]
        self.angle += self.angle_vel

        # wrap arround the screen
        self.pos[0] = self.pos[0] % WIDTH
        self.pos[1] = self.pos[1] % HEIGHT


def draw(canvas):
    global time

    # animiate background
    time += 1
    wtime = (time / 4) % WIDTH
    center = debris_info.get_center()
    size = debris_info.get_size()
    canvas.draw_image(nebula_image, nebula_info.get_center(), nebula_info.get_size(), [WIDTH / 2, HEIGHT / 2], [WIDTH, HEIGHT])
    canvas.draw_image(debris_image, center, size, (wtime - WIDTH / 2, HEIGHT / 2), (WIDTH, HEIGHT))
    canvas.draw_image(debris_image, center, size, (wtime + WIDTH / 2, HEIGHT / 2), (WIDTH, HEIGHT))

    # draw ship and sprites
    my_ship.draw(canvas)
    a_rock.draw(canvas)
    if None != a_missile:
        a_missile.draw(canvas)

    # update ship and sprites
    my_ship.update()
    a_rock.update()
    if None != a_missile:
        a_missile.update()

    # user interface
    canvas.draw_text('lives: ' + str(lives), (20, 20), 20, 'White')
    canvas.draw_text('score: ' + str(score), (WIDTH - 90, 20), 20, 'White')

def keydown_left():
    my_ship.decrement_angle_vel(0.1)

def keyup_left():
    my_ship.increment_angle_vel(0.1)

def keydown_right():
    my_ship.increment_angle_vel(0.1)

def keyup_right():
    my_ship.decrement_angle_vel(0.1)

def keydown_up():
    my_ship.enable_thrusters(True)

def keyup_up():
    my_ship.enable_thrusters(False)

def keydown_space():
    my_ship.shoot()

key_bind = {
    simplegui.KEY_MAP['left']  : (keydown_left, keyup_left),
    simplegui.KEY_MAP['right'] : (keydown_right, keyup_right),
    simplegui.KEY_MAP['up']    : (keydown_up, keyup_up),
    simplegui.KEY_MAP['space'] : (keydown_space, None)
}

HANDLER_DOWN = 0
HANDLER_UP = 1

def key_handler(key, handler_id):
    if key_bind.has_key(key):
        handler = key_bind[key][handler_id] # take handler for keydown event
        if None != handler:
            handler()

def keydown_handler(key):
    key_handler(key, HANDLER_DOWN)

def keyup_handler(key):
    key_handler(key, HANDLER_UP)


# timer handler that spawns a rock
def rock_spawner():
    global a_rock

    pos = (WIDTH * random.random(), HEIGHT * random.random())
    ang = random.random() * math.pi * 2 # 2pi
    vel = angle_to_vector(ang)
    ang_vel = random.random() * 0.1
    img = asteroid_image
    info = asteroid_info

    a_rock = Sprite(pos, vel, ang, ang_vel, img, info)

# initialize frame
frame = simplegui.create_frame("Asteroids", WIDTH, HEIGHT)

# initialize ship and two sprites
my_ship = Ship([WIDTH / 2, HEIGHT / 2], [0, 0], 0, ship_image, ship_info)
a_rock = Sprite([WIDTH / 3, HEIGHT / 3], [1, 1], 0, 0, asteroid_image, asteroid_info)
a_missile = None

# register handlers
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown_handler)
frame.set_keyup_handler(keyup_handler)

timer = simplegui.create_timer(1000.0, rock_spawner)

# get things rolling
timer.start()
frame.start()
